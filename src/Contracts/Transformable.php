<?php
/**
 * Created by PhpStorm.
 * User: Bilal Younas
 * Date: 10/26/2019
 * Time: 11:56 PM
 */

namespace CollectiveConscious\RepositoryDesignPattern\Contracts;


interface Transformable
{
    /**
     * @return array
     */
    public function transform();
}