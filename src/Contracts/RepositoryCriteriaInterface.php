<?php
/**
 * Created by PhpStorm.
 * User: Bilal Younas
 * Date: 10/26/2019
 * Time: 6:18 PM
 */

namespace CollectiveConscious\RepositoryDesignPattern\Contracts;

use CollectiveConscious\RepositoryDesignPattern\Criteria;
use Illuminate\Support\Collection;

interface RepositoryCriteriaInterface
{
    /**
     * Push Criteria for filter the query
     *
     * @param $criteria
     *
     * @return $this
     */
    public function pushCriteria($criteria);
    /**
     * Pop Criteria
     *
     * @param $criteria
     *
     * @return $this
     */
    public function popCriteria($criteria);
    /**
     * Get Collection of Criteria
     *
     * @return Collection
     */
    public function getCriteria();
    /**
     * Find data by Criteria
     *
     * @param CriteriaInterface $criteria
     *
     * @return mixed
     */
    public function getByCriteria(CriteriaInterface $criteria);
    /**
     * Skip Criteria
     *
     * @param bool $status
     *
     * @return $this
     */
    public function skipCriteria($status = true);
    /**
     * Reset all Criterias
     *
     * @return $this
     */
    public function resetCriteria();
}