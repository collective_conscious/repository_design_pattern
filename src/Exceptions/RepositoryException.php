<?php
/**
 * Created by PhpStorm.
 * User: Bilal Younas
 * Date: 10/26/2019
 * Time: 11:40 PM
 */

namespace CollectiveConscious\RepositoryDesignPattern\Exceptions;

use Exception;

class RepositoryException extends Exception
{

}