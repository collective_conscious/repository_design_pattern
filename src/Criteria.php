<?php
/**
 * Created by PhpStorm.
 * User: Bilal Younas
 * Date: 10/26/2019
 * Time: 7:10 AM
 */

namespace CollectiveConscious\RepositoryDesignPattern;


use CollectiveConscious\RepositoryDesignPattern\Contracts\RepositoryInterface;
use Illuminate\Http\Request;

abstract class Criteria
{
    /**
     * @var \Illuminate\Http\Request
     */
    protected $request;

    public function __construct(Request $request)
    {
        $this->request = $request;
    }

    /**
     * @param $model
     * @param RepositoryInterface $repository
     * @return mixed
     */
    public abstract function apply($model, RepositoryInterface $repository);
}